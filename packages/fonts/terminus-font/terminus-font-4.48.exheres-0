# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

FONT="terminus"

require xfont sourceforge [ suffix=tar.gz ]

SUMMARY="A clean fixed width font for x11 and terminal"

LICENCES="
    OFL-1.1 [[ note = [ font files ] ]]
    GPL-2   [[ note = [ installation scripts ] ]]
"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="X"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        X? ( x11-apps/bdftopcf )
"

src_configure() {
    edo ./configure \
        --prefix=/usr \
        --x11dir=/usr/share/fonts/X11/${FONT}/
}

src_compile() {
    emake psf

    if option X ; then
        nonfatal emake pcf || die "emake pcf failed"
    fi
}

src_install() {
    emake DESTDIR="${IMAGE}" install-psf

    if option X ; then
        nonfatal emake DESTDIR="${IMAGE}" install-pcf || die "emake install pcf failed"
    fi

    dodoc README README-BG CHANGES
}

pkg_postrm() {
    if option X ; then
        xfont_pkg_postrm
    fi
}

pkg_postinst() {
    if option X ; then
        xfont_pkg_postinst
    fi
}

