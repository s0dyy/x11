# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=anholt ] meson [ meson_minimum_version=0.54.0 ]

SUMMARY="Epoxy is a library for handling OpenGL function pointer management"
DESCRIPTION="
Epoxy is a library for handling OpenGL function pointer management by hiding
the complexity of dlopen(), dlsym(), glXGetProcAddress(), ... from the
developer.

Features:
* Automatically initializes as new GL functions are used.
* GL 4.4 core and compatibility context support.
* GLES 1/2/3 context support.
* Knows about function aliases so (e.g.) glBufferData() can be
  used with GL_ARB_vertex_buffer_object implementations, along
  with GL 1.5+ implementations.
* EGL, GLX, and WGL support.
* Can be mixed with non-epoxy GL usage.
"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    doc
    X [[ description = [ GLX support ]
         presumed = true ]]
    ( libc: musl )
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        virtual/pkg-config
        x11-utils/util-macros[>=1.8]
        doc? (
            app-doc/doxygen
            media-gfx/graphviz
        )
        X? (
            x11-libs/libX11 [[ note = [ Only needs Xlib.h for GLX ] ]]
        )
        !libc:musl? (
            dev-libs/libglvnd[X?] [[ note = [ eglplatform.h and GL.h. khrplatform.h for tests ] ]]
        )
        libc:musl? (
            x11-dri/mesa[X?] [[ note = [ same as above, but libglvnd doesn't work on musl ] ]]
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Degl=yes
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc docs'
    'X glx yes no'
    'X x11'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

