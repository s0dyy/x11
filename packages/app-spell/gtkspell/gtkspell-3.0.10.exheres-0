# Copyright 2009, 2010 Mike Kelly
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ pnv=${PN}3-${PV} suffix=tar.xz ]
require vala [ vala_dep=true ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="spelling suggestions for GTK widgets"
DESCRIPTION="
GtkSpell provides word-processor-style highlighting and replacement of
misspelled words in a GtkTextView widget. Right-clicking a misspelled
word pops up a menu of suggested replacements.
"

LICENCES="GPL-2"
SLOT="3.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc gobject-introspection
    vapi [[ requires = [ gobject-introspection ] ]]
    ( linguas: af ak ast be cs da de el eo es eu ff fi fo fr
               ga gl he hr hu hy id is it ja ky lg lt lv mn ms
               nb nl nso pl pt pt_BR rm ro ru rw sk sl son sq
               sr st sv th tr uk vi wa zh_CN zh_HK zh_TW zu )
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext
        doc? ( dev-doc/gtk-doc )
    build+run:
        x11-libs/gtk+:3[gobject-introspection?]
        app-spell/enchant:2
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.30.0] )
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'doc gtk-doc'
    'gobject-introspection introspection'
    'vapi vala'
)
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-gtk3
    --enable-nls
    --disable-gtk2
    --disable-static
)

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/ChangeLog"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/doc/"

WORK="${WORKBASE}/${PN}3-${PV}"

src_prepare() {
    edo intltoolize --force --automake

    autotools_src_prepare
}

