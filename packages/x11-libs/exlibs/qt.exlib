# Copyright 2008-2010 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2008, 2009, 2010, 2014 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'qt-4.3.4-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

myexparam pn=${MY_PN:-${PN}-everywhere-src}
myexparam pv=${MY_PV:-${PV}}
MY_PNV=$(exparam pn)-$(exparam pv)

require toolchain-funcs

myexparam -b docs=true

export_exlib_phases src_compile src_install

HOMEPAGE="https://www.qt.io/"
if [[ ${PV} == *-@(alpha|beta|rc|RC)* ]] ; then
    BASE_URI="mirror://qt/development_releases/qt/$(ever range 1-2)/${PV/rc1/rc}/"
else
    BASE_URI="mirror://qt/official_releases/qt/$(ever range 1-2)/${PV}/"
fi

if ever is_scm; then
    SCM_REPOSITORY="git://code.qt.io/qt/${PN}.git"
    SCM_BRANCH="5.9"

    require scm-git

    SLOT=5
elif ever at_least 5.2.1; then
    DOWNLOADS="${BASE_URI}submodules/${MY_PNV}.tar.xz"
    # Minor version specific documentation is gone
    UPSTREAM_DOCUMENTATION="https://doc.qt.io/qt-$(ever major)/index.html [[ description = [ Qt Reference Documentation ] ]]"
else
    DOWNLOADS="${BASE_URI}${MY_PNV}.tar.gz"
    UPSTREAM_CHANGELOG="${BASE_URI}changes-${PV}"
    UPSTREAM_DOCUMENTATION="https://doc.qt.io/qt-$(ever range 1-2)/index.html [[ description = [ Qt Reference Documentation ] ]]"
fi

if ! ever is_scm; then
    SLOT=$(ever major)
    WORK=${WORKBASE}/${MY_PNV}
fi

LICENCES="|| ( GPL-2 LGPL-3 ) FDL-1.3"

DEFAULT_SRC_INSTALL_PARAMS+=( INSTALL_ROOT="${IMAGE}" )

qt_enable() {
    local opt="${1}" feature="${2:-${1}}"
    local prefix="${3:+-}${3}" postfix="${4:+-}${4}"
    (( ${#} >= 4 )) && shift 4 || shift ${#}

    if option "${opt}"; then
        echo "${prefix}-${feature}${postfix}" "${@}"
    else
        echo "-no-${feature}"
    fi
}

qt_feature() {
    local opt="${1}" feature="${2:-${1}}"

    if option "${opt}" ; then
        echo "-feature-${feature}"
    else
        echo "-no-feature-${feature}"
    fi
}

qt_build() {
    local opt="${1}" feature="${2:-${1}}"

    if option "${opt}" ; then
        echo "-make ${feature}"
    else
        echo "-nomake ${feature}"
    fi
}

if exparam -b docs ; then
    MYOPTIONS+="
        doc [[ description = [ Build API documentation ] ]]
    "

    DEPENDENCIES+="
        build:
            doc? ( x11-libs/qttools:5   [[ note = qhelpgenerator ]] )
    "
fi

qt_src_compile() {
    default

    if exparam -b docs; then
        if option doc; then
            emake docs
        fi
    fi
}

qt_src_install() {
    default

    if exparam -b docs; then
        if option doc; then
            emake install_docs INSTALL_ROOT="${IMAGE}"
        fi
    fi

    # remove references to build dir in case there are prl files
    if compgen -G "${IMAGE}/usr/$(exhost --target)/lib/libQt5*.prl"; then
        edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/$(exhost --target)/lib/libQt5*.prl
    fi
}


