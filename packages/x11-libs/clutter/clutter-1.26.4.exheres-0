# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require clutter autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 1.13 ] ]

LICENCES="LGPL-2.1"
SLOT="1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc
    gobject-introspection
    gtk-doc
    X
    wayland
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.17]
        virtual/pkg-config[>=0.20]
        x11-proto/xorgproto
        doc? ( dev-libs/libxslt )
        gtk-doc? ( dev-doc/gtk-doc[>=1.20] )
    build+run:
        core/json-glib[>=0.12][gobject-introspection?]
        dev-libs/atk[>=2.5.3][gobject-introspection?]
        dev-libs/glib:2[>=2.44.0]
        gnome-desktop/libgudev[>=136]
        media-libs/fontconfig
        sys-libs/libinput[>=0.19.0]
        x11-dri/mesa[?X]
        x11-libs/cairo[>=1.14.0]
        x11-libs/cogl:1.0[>=1.21.2][gobject-introspection?][wayland?][X?]
        x11-libs/gtk+:3[>=3.3.18][gobject-introspection?]
        x11-libs/libxkbcommon
        x11-libs/pango[>=1.30][gobject-introspection?]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.39.0] )
        wayland? (
            sys-libs/wayland
            x11-libs/gdk-pixbuf:2.0
        )
        X? (
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXfixes[>=3]
            x11-libs/libXdamage
            x11-libs/libXcomposite[>=0.4]
            x11-libs/libXi
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    test:
        x11-libs/gdk-pixbuf:2.0
"

RESTRICT="test" # FIXME: requires X

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/clutter-no-x.patch )

# tests are disabled, so don't enable conformance
DEFAULT_SRC_CONFIGURE_PARAMS=( '--enable-conformance=no' '--enable-evdev-input' '--enable-gdk-backend'
                               '--enable-egl-backend' '--disable-examples' '--disable-Werror' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'doc docs' 'gtk-doc' 'gobject-introspection introspection'
                                       'wayland wayland-backend' 'wayland wayland-compositor'
                                       'X x11-backend' 'X xinput' )

