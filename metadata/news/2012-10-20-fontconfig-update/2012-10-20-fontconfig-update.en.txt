Title: fontconfig config file path move
Author: Saleem Abdulrasool <compnerd@compnerd.org>
Content-Type: text/plain
Posted: 2012-10-20
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: media-libs/fontconfig[>=2.10.1]

The location of system provided configuration files has changed from
/etc/fonts/conf.avail/ to /usr/share/fontconfig/conf.avail/ in order to follow
the filesystem hierarchy specification (FHS) more closely.

Packages providing fontconfig configuration files should be reinstalled.

    # cave resolve -1x $(cave print-owners -f '%c/%p ' /etc/fonts/conf.avail)

You will also need to update the orphaned symbolic links that exist in
/etc/fonts/conf.d/ to point to the new location.

    # find -L /etc/fonts/conf.d/ -type l -execdir sh -c 'ln -sf $(readlink -f /usr/share/fontconfig/conf.avail/{}) {}' \;

User configuration has also moved from ~/.fonts.conf to
${XDG_CONFIG_DIR}/fontconfig/fonts.conf (usually,
~/.config/fontconfig/fonts.conf).  The old path will be honoured currently, but
is deprecated and will be dropped in a future release.

It is now also possible to manage user specific configuration similar to system
configuration via ${XDG_CONFIG_DIR}/fontconfig/conf.d 

