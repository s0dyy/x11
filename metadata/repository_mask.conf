(
    dev-libs/libepoxy[~scm]
    media-libs/glew[~scm]
    x11-apps/xinit[~scm]
    x11-apps/xkeyboard-config[~scm]
    x11-dri/glu[~scm]
    x11-dri/libdrm[~scm]
    x11-dri/mesa[~scm]
    x11-dri/mesa-demos[~scm]
    x11-drivers/glamor[~scm]
    x11-drivers/intel-driver[~scm]
    x11-drivers/xf86-input-evdev[~scm]
    x11-drivers/xf86-input-keyboard[~scm]
    x11-drivers/xf86-input-libinput[~scm]
    x11-drivers/xf86-input-mouse[~scm]
    x11-drivers/xf86-input-synaptics[~scm]
    x11-drivers/xf86-input-wacom[~scm]
    x11-drivers/xf86-video-amdgpu[~scm]
    x11-drivers/xf86-video-ati[~scm]
    x11-drivers/xf86-video-dummy[~scm]
    x11-drivers/xf86-video-intel[~scm]
    x11-drivers/xf86-video-nouveau[~scm]
    x11-drivers/xf86-video-virgl[~scm]
    x11-drivers/xf86-video-wayland[~scm]
    x11-misc/rofi[~scm]
    x11-libs/libevdev[~scm]
    x11-libs/libpciaccess[~scm]
    x11-libs/libX11[~scm]
    x11-libs/libXext[~scm]
    x11-libs/libXi[~scm]
    x11-libs/libxkbcommon[~scm]
    x11-libs/pixman[~scm]
    x11-libs/virglrenderer[~scm]
    x11-proto/dri2proto[~scm]
    x11-proto/fontsproto[~scm]
    x11-proto/inputproto[~scm]
    x11-proto/randrproto[~scm]
    x11-proto/xextproto[~scm]
    x11-proto/xproto[~scm]
    x11-server/xorg-server[~scm]
    x11-themes/oxygen-gtk2[~scm]
    x11-themes/oxygen-gtk3[~scm]
    x11-utils/util-macros[~scm]
    x11-utils/xcb-util[~scm]
    x11-wm/fluxbox[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm versions ]
]]

media-libs/freetype:2[<2.10.4] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 20 Oct 2020 ]
    token = security
    description = [ CVE-2020-15999 ]
]]

x11-apps/xkeyboard-config[<=2.4.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jan 2012 ]
    token = security
    description = [ CVE-2012-0064 ]
]]

app-text/poppler[<0.56.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Jun 2017 ]
    token = security
    description = [ CVE-2017-9775 ]
]]

(
    x11-server/xorg-server[<1.20.11]
    x11-server/xwayland[<21.1.1]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 13 Apr 2021 ]
    *token = security
    *description = [ CVE-2021-3472 ]
]]

x11-libs/libXv[<1.0.8] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1989 and CVE-2013-2066 ]
]]

x11-libs/libXrandr[<1.4.1] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1986 ]
]]

x11-libs/libXext[<1.3.2] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1982 ]
]]

x11-libs/libXtst[<1.2.2] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-2063 ]
]]

x11-libs/libXxf86vm[<1.1.3] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-2001 ]
]]

x11-libs/libXt[<1.1.4] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-2002 and CVE-2013-2005 ]
]]

x11-libs/libXres[<1.0.7] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1988 ]
]]

x11-libs/libXinerama[<1.1.3] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1985 ]
]]

x11-libs/libXcursor[<1.1.15] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Nov 2017 ]
    token = security
    description = [ CVE-2017-16612 ]
]]

x11-libs/libXfixes[<5.0.1] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1983 ]
]]

x11-libs/libXp[<1.0.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2013 ]
    token = security
    description = [ CVE-2013-2062 ]
]]

x11-libs/libXxf86dga[<1.1.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2013 ]
    token = security
    description = [ CVE-2013-1991 and CVE-2013-2000 ]
]]

x11-libs/libdmx[<1.1.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2013 ]
    token = security
    description = [ CVE-2013-1992 ]
]]

x11-libs/libFS[<1.0.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2013 ]
    token = security
    description = [ CVE-2013-1996 ]
]]

x11-libs/libX11[<1.7.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 21 May 2021 ]
    token = security
    description = [ CVE-2021-31535 ]
]]

x11-libs/libxcb[<1.9.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Jun 2013 ]
    token = security
    description = [ CVE-2013-2064 ]
]]

x11-libs/libXi[<1.7.2] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 05 Jul 2013 ]
    token = security
    description = [ CVE-2013-1998, CVE-2013-1984, CVE-2013-1995 ]
]]

x11-libs/libXrender[<0.9.8] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 05 Jul 2013 ]
    token = security
    description = [ CVE-2013-1987 ]
]]

x11-libs/libXvMC[<1.0.8] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 05 Jul 2013 ]
    token = security
    description = [ CVE-2013-1999 ]
]]

x11-libs/libXfont[<1.5.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Nov 2017 ]
    token = security
    description = [ CVE-2017-16611 ]
]]

net-libs/libvncserver[<0.9.13] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Sep 2020 ]
    token = security
    description = [ CVE-2018-21247, CVE-2019-208{39,40}, CVE-2020-1439{6,7,8,9},
                    CVE-2020-1440{0,1,2,3,4,5} ]
]]

x11-libs/libvdpau[<1.1.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 01 Sep 2015 ]
    token = security
    description = [ CVE-2015-5{198,199,200} ]
]]

(
    x11-libs/qtwebengine:5[<5.12.5]
    x11-libs/qtwebengine:5[>5.13&<5.13.1]
)
[[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 11 Sep 2019 ]
    *token = security
    *description = [ CVE-2019-{5829,5831,5832,5837,5839,5842,5851,5852,5854,
                         5855,5856,5857,5860,5861,5862,5865}
                     Security bugs 934161,939644,948172,948228,948944,950005,
                         952849,956625,958457,958689,959193,959518,958717,
                         960785,961674,961597,962083,964002,973893,974627,
                         976050,977057,981602,983850,983938 ]
]]

media-libs/fontconfig[<2.12.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 Aug 2016 ]
    token = security
    description = [ CVE-2016-5384 ]
]]

media-libs/fontconfig[~>2.13.91] [[
    author = [ Alexander Kapshuna <kapsh@kap.sh> ]
    date = [ 18 Jul 2019 ]
    token = pre-release
    description = [ Development release ]
]]

x11-libs/libXpm[<3.5.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2016-10164 ]
]]

x11-libs/libXfont2[<2.0.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Nov 2017 ]
    token = security
    description = [ CVE-2017-16611 ]
]]

(
    x11-libs/qtbase[<5.11.3]
    x11-libs/qtimageformats[<5.11.3]
    x11-libs/qtvirtualkeyboard[<5.11.3]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 04 Dec 2018 ]
    *token = security
    *description = [ https://blog.qt.io/blog/2018/12/04/qt-5-11-3-released-important-security-updates/ ]
]]

x11-apps/xdm[<1.1.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Mar 2019 ]
    token = security
    description = [ CVE-2013-2179 ]
]]

x11-libs/libXdmcp[<1.1.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 18 Mar 2019 ]
    token = security
    description = [ CVE-2017-2625 ]
]]

(
    x11-libs/qtbase:5[<5.12.6]
    x11-libs/qtbase:5[>=5.13&<5.14.1]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 27 Jan 2019 ]
    *token = security
    *description = [ CVE-2020-0569, CVE-2020-0570 ]
]]

media-gfx/fontforge[<20200314] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Sep 2020 ]
    token = security
    description = [ CVE-2020-5395, CVE-2020-5496 ]
]]

x11-libs/gdk-pixbuf[<2.42.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 Jan 2021 ]
    token = security
    description = [ CVE-2020-29385 ]
]]

x11-libs/qtsvg:5[<5.15.2_p5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 Apr 2021 ]
    token = security
    description = [ CVE-2021-3481 ]
]]
